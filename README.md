[PL]
Aplikacja przeszukujaca Wikipedie (wersja angielska), oraz wyswietlajaca wyniki zapytania.
Przy jej towrzeniu zostaly obrane dwie drogi. 
Pierwsza to droga Backendowa (PHP) natomiast druga to droga czysto Frontendowa (JavaScript)
Do uruchomienia odpowiedniej drogi zostaly przypisane dwa przyciski - "Search!" w domysle backend oraz "Search Front!" 

Dodatkowo zostaly zaimplementowane testy jednostkowe:
QUnit - test JS - dostepny pod adresem wikisearch/?action=test
PHPUnit - test PHP - dostepny z poziomu np PHP Storm

Punkt dostepu do aplikacji: wikisearch\httpdocs\index.php

[EN]
WikiSearch is a web application that uses Wikipedia to provide search engine and displaying results.
While creating this app I've used two approces. First one was the Frontend (Java Script) method that used
AJAX to get results and to display results using JS object. The secound approach was made in Backend (PHP OOP) method.
I've generated the question with cURL and displaying the result using symfony (Twig).
Both methods have unit tests - PHPUnit and QUnit test respectivly for back and front.

Entry point to the app is wikisearch\httpdocs\index.php
Entry point to the Qunit test wikisearch\httpdocs\index.php\?Action=test