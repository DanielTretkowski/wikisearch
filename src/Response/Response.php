<?php

namespace Sda\Wikisearch\Response;


class Response
{

    /**
     * @param string $searchQuote
     * @return array
     */
    public function sendAll($searchQuote)
    {
        $url1 = "https://en.wikipedia.org/w/api.php?action=opensearch&search=+$searchQuote+&format=json&limit=20";
        $url2 = 'https://en.wikipedia.org/w/api.php?action=query&format=json&prop=pageimages'.
        '%7Cpageterms&generator=prefixsearch&redirects=1&formatversion=2&piprop=thumbnail&pithumbsize=250'.
        "&pilimit=20&wbptterms=description&gpssearch=+$searchQuote+&gpslimit=20";

        $resultArray =[
        $this->sendQueryToWikipediaApi($url1),
        $this->sendQueryToWikipediaApi($url2)
            ];

        return $resultArray;
    }

    /**
     * @param string $url
     * @return array
     */
    private function sendQueryToWikipediaApi($url)
    {

        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, $url);
        curl_setopt($cURL, CURLOPT_HTTPGET, true);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($cURL);
        $err = curl_error($cURL);
        curl_close($cURL);

        if ($err) {
            die('Problem z wysylka');
        }

        return json_decode($response, true);
    }

}