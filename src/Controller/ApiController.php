<?php

namespace Sda\Wikisearch\Controller;

use Sda\Wikisearch\Request\Request;
use Sda\Wikisearch\View\Template;
use Sda\Wikisearch\Response\Response;
use Sda\Wikisearch\Result\ResultRepository;

class ApiController
{

    /**
     * @var Request
     */
    private $request;
    /**
     * @var Template
     */
    private $template;
    /**
     * @var string $templateName
     */
    private $templateName = '';
    /**
     * @var array $params
     */
    private $params = [];
    /**
     * @var Response
     */
    private $response;
    /**
     * @var ResultRepository
     */
    private $resultRepository;


    /**
     * @param Request $request
     * @param Template $template
     * @param Response $response
     * @param ResultRepository $resultRepository
     */
    public function __construct
    (
        Request $request,
        Template $template,
        Response $response,
        ResultRepository $resultRepository
    )
    {
        $this->request = $request;
        $this->template = $template;
        $this->response = $response;
        $this->resultRepository = $resultRepository;
    }

    public function run()
    {

        $action = $this->request->getParamFromGet('action', 'index');

        switch ($action) {
            case 'index':
                $this->templateName = 'index.html';
                break;
            case 'Results':
               $search = str_replace(' ','',$this->request->getParamFromPost('search'));
                $resultsArray= $this->response->sendAll($search);
                $resultsObjectCollection = $this->resultRepository->buildResultsCollection($resultsArray);
                $this->params=[

                    'results'=> $resultsObjectCollection
                ];
                $this->templateName = 'result.html';
                break;
            case 'test':
                $this->templateName = 'test.html';
                break;
            default :
                echo '404';
                break;
        }

        $this->template->renderTemplate($this->templateName, $this->params);
    }

}
