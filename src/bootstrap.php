<?php


use Sda\Wikisearch\Controller\ApiController;
use Sda\Wikisearch\Request\Request;
use Sda\Wikisearch\Config\Config;
use Sda\Wikisearch\View\Template;
use Sda\Wikisearch\Response\Response;
use Sda\Wikisearch\Result\ResultRepository;

require_once __DIR__ . '/../vendor/autoload.php';


$loader = new Twig_Loader_Filesystem(Config::TEMPLATE_DIR);
$twig = new Twig_Environment(
    $loader,
    [
        'cache' => false,
        'debug' => true
    ]
);


$template = new Template($twig);
$request = new Request();
$response = new Response();
$resultRepository = new ResultRepository();

$app = new ApiController
(
    $request,
    $template,
    $response,
    $resultRepository
);
$app->run();