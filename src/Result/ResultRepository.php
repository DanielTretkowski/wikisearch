<?php

namespace Sda\Wikisearch\Result;


class ResultRepository
{

    /**
     * @param array $wikiJson
     * @return ResultCollection
     */
    public function buildResultsCollection(array $wikiJson)
    {
        $wikidiscription = $wikiJson[0];
        $wikiImage = $wikiJson[1];
        $results = new ResultCollection();
        for ($i = 0; $i < count($wikidiscription[1]); $i++) {

            $builder = new ResultBuilder();
            $result = $builder
                ->withHeader($wikidiscription[1][$i])
                ->withSneakPeak($wikidiscription[2][$i])
                ->withUrl($wikidiscription[3][$i])
                ->build();
            $results->add($result);
        }

        foreach ($results as $row) {
            for ($i = 0; $i < count($wikidiscription[1]); $i++) {
                if ($row->getHeader() === $wikiImage['query']['pages'][$i]['title']) {
                    if (array_key_exists(('thumbnail'), $wikiImage['query']['pages'][$i])) {
                        $row->setImage($wikiImage['query']['pages'][$i]['thumbnail']['source']);
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            }
        }

        return $results;
    }
}