<?php

namespace Sda\Wikisearch\Result;

class Result implements \JsonSerializable
{
    /**
     * @var string $header
     */
    private $header;
    /**
     * @var string $image
     */
    private $image;
    /**
     * @var string $sneakPeak
     */
    private $sneakPeak;
    /**
     * @var string $url
     */
    private $url;

    /**
     * @param string $image
     * @param string $header
     * @param string $sneakPeak
     * @param string $url
     */
    public function __construct
    (
        $header,
        $sneakPeak,
        $url,
        $image
    )
    {

        $this->header = $header;
        $this->image = $image;
        $this->sneakPeak = $sneakPeak;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param string $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getSneakPeak()
    {
        return $this->sneakPeak;
    }

    /**
     * @param string $sneakPeak
     */
    public function setSneakPeak($sneakPeak)
    {
        $this->sneakPeak = $sneakPeak;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return
            [
                'header'=> $this->header,
                'sneakPeak'=> $this->sneakPeak,
                'url'=> $this->url,
                'image' => $this->image
            ];
    }
}