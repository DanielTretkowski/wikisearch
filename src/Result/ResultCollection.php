<?php

namespace Sda\Wikisearch\Result;

use Sda\Wikisearch\TypedCollection;

class ResultCollection extends TypedCollection
{

    public function __construct()
    {
        $this->setItemType(Result::class);
    }
}