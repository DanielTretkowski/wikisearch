<?php


namespace Sda\Wikisearch\Result;




class ResultBuilder
{
    /**
     * @var string
     */
    private $header ='none';
    /**
     * @var string
     */
    private $image = 'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg';
    /**
     * @var string
     */
    private $sneakPeak = 'none';
    /**
     * @var string
     */
    private $url = 'https://pl.wikipedia.org/wiki/Wikipedia:Strona_g%C5%82%C3%B3wna';

    /**
     * @return Result
     */
    public function build()
    {

        return new Result
        (
            $this->header,
            $this->sneakPeak,
            $this->url,
            $this->image
        );
    }

    /**
     * @param string $header
     * @return ResultBuilder
     */
    public function withHeader($header)
    {
        $this->header = $header;
        return $this;
    }

    /**
     * @param string $image
     * @return ResultBuilder
     */
    public function withImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @param string $url
     * @return ResultBuilder
     */
    public function withUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param string $sneakPeak
     * @return ResultBuilder
     */
    public function withSneakPeak($sneakPeak)
    {
        $this->sneakPeak = $sneakPeak;
        return $this;
    }
}