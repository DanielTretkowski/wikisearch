<?php

use Sda\Wikisearch\Result\Result;


class ResultTest extends PHPUnit_Framework_TestCase
{

    protected $result;


    protected function setUp()
    {
        $this->result = new Result('test', 'test', 'test', 'test');
    }

    public function testResultGetHeader()
{
    $this->assertEquals('test', $this->result->getHeader());
}

    public function testResultSetHeader()
    {
        $this->result->setHeader('daniel');
        $this->assertEquals('daniel', $this->result->getHeader());
    }
    public function testResultGetImage()
    {
        $this->assertEquals('test', $this->result->getImage());
    }

    public function testResultSetImage()
    {
        $this->result->setImage('daniel');
        $this->assertEquals('daniel', $this->result->getImage());
    }
    public function testResultGetSneakPeak()
    {
        $this->assertEquals('test', $this->result->getSneakPeak());
    }

    public function testResultSetSneakPeak()
    {
        $this->result->setSneakPeak('daniel');
        $this->assertEquals('daniel', $this->result->getSneakPeak());
    }
    public function testResultGetUrl()
    {
        $this->assertEquals('test', $this->result->getUrl());
    }

    public function testResultSetUrl()
    {
        $this->result->setUrl('daniel');
        $this->assertEquals('daniel', $this->result->getUrl());
    }
}
