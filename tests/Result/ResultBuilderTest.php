<?php

use Sda\Wikisearch\Result\ResultBuilder;

class ResultBuilderTest extends PHPUnit_Framework_TestCase
{

    protected $resultBuilder;
    protected $testObject;

    public function setUp()
    {
        $this->resultBuilder = new ResultBuilder();
        $this->testObject = $this->resultBuilder
            ->withHeader('daniel')
            ->withSneakPeak('daniel')
            ->withUrl('daniel')
            ->withImage('daniel')
            ->build();
    }

    public function testResultBuilderWithHeader()
    {
        $this->assertEquals('daniel', $this->testObject->getHeader());
    }

    public function testResultBuilderSneakPeak()
    {
        $this->assertEquals('daniel', $this->testObject->getSneakPeak());
    }

    public function testResultBuilderUrlk()
    {
        $this->assertEquals('daniel', $this->testObject->getUrl());
    }

    public function testResultBuilderImage()
    {
        $this->assertEquals('daniel', $this->testObject->getImage());
    }
}
