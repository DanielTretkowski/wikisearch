$(document).ready(function () {

    $('#searchParameter').autocomplete({
        minLength: 3,
        source: function (request, response) {
            $.ajax({
                url: "http://en.wikipedia.org/w/api.php",
                dataType: "jsonp",
                data: {
                    'action': "opensearch",
                    'format': "json",
                    'search': request.term
                },
                success: function (data) {
                    response(data[1]);
                },
                error: function () {
                    $(".results").html("Something went wrong");
                }
            })
        }
    });

    $('#subButton').click(function (e) {
        var searchParameter = $('#searchParameter').val();

        $('#MainForm').animate({marginTop: "0%"}, 500);
        $.ajax({
            type: "POST",
            url: "?action=Results",
            data: {search: searchParameter},

            success: function (data) {
                $('.results').html(data).fadeIn();
            },
            error: function () {
                $(".results").html("Something went wrong");
            }
        });

        e.preventDefault();
    });

    $('#subButtonFront').click(function (e) {
        var searchParameter = $('#searchParameter').val();

        $('#MainForm').animate({marginTop: "0%"}, 500);

        $.ajax({
            method: "GET",
            url: 'https://en.wikipedia.org/w/api.php?action=query&inprop=url&format=json&pithumbsize=250&gsrlimit=20' +
            "&generator=search&prop=pageimages|extracts|info&exintro&explaintext&exsentences=1&origin=*&gsrsearch=" + searchParameter,
            dataType: "Jsonp",

            success: function (data) {
                $(".results").html("");

                if (data.query != null && data.query.pages != null) {

                    var pages = data.query.pages;
                    var result = $('.results');

                    for (pageId in pages) {

                        var page = pages[pageId];

                        var resultBuilder = new ResultBuilder();
                        resultBuilder.setHeader(page.title);
                        resultBuilder.setSneakPeak(page.extract);
                        resultBuilder.setUrl(page.fullurl);
                        if (page.thumbnail != null && page.thumbnail.source != null) {
                            resultBuilder.setImage(page.thumbnail.source);
                        } else {
                            resultBuilder.setImage("https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg");
                        }
                        result.append(resultBuilder.build());
                    }
                }else{
                    $(".results").html("Pls add query");
                }
            },
            error: function () {
                $(".results").html("Something went wrong");
            }
        });
        e.preventDefault();
    });
});