class ResultBuilder {

    constructor() {
        this.$result = $("#resultJS").clone();
        this.$result.removeAttr('id');
        this.$result.show();
    }

    setHeader(header) {
        this.$result.find(".header").text(header);
    }

    setSneakPeak(sneakPeak) {
        this.$result.find(".sneakPeak").text(sneakPeak);
    }

    setImage(image) {
        this.$result.find("img").attr("src", image);
    }

    setUrl(url) {
        this.$result.find("a").attr("href", url);
    }

    build() {
        return this.$result;
    }
}